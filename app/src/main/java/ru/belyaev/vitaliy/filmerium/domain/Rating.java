package ru.belyaev.vitaliy.filmerium.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rating {

    @SerializedName("Source")
    @Expose
    private String source;

    @SerializedName("Value")
    @Expose
    private String value;

    public String getSource() {
        return source;
    }

    public String getValue() {
        return value;
    }
}
