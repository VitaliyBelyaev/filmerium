package ru.belyaev.vitaliy.filmerium;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ru.belyaev.vitaliy.filmerium.domain.MoviePreview;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieVH> {

    interface MovieOnClickHandler {
        void onClick(int position, String imdbID);

    }

    class MovieVH extends RecyclerView.ViewHolder {

        ImageView poster;
        TextView title;
        TextView year;


        MovieVH(View itemView) {
            super(itemView);

            poster = itemView.findViewById(R.id.movie_poster);
            title = itemView.findViewById(R.id.tv_movie_title);
            year = itemView.findViewById(R.id.tv_movie_year);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    onClickHandler.onClick(position, moviePreviews.get(position).getImdbID());
                }
            });
        }
    }

    private ArrayList<MoviePreview> moviePreviews;
    private ArrayList<MoviePreview> defaultOrder;
    private MovieOnClickHandler onClickHandler;


    public MoviesAdapter(MovieOnClickHandler onClickHandler) {
        this.onClickHandler = onClickHandler;
    }


    @NonNull
    @Override
    public MovieVH onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new MovieVH(inflater.inflate(R.layout.movie_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MovieVH holder, int position) {

        MoviePreview moviePreview = moviePreviews.get(position);

        Picasso.get()
                .load(moviePreview.getPoster())
                .placeholder(R.drawable.image_placeholder)
                .into(holder.poster);

        holder.title.setText(moviePreview.getTitle());
        holder.year.setText(moviePreview.getYear());
    }

    @Override
    public int getItemCount() {
        if (moviePreviews != null) {
            return moviePreviews.size();
        }
        return 0;
    }

    public void setDefaultOrderedAlbums(List<MoviePreview> moviePreviews) {
        this.defaultOrder = (ArrayList<MoviePreview>) moviePreviews;
    }

    public void replaceWith(List<MoviePreview> moviePreviews) {
        this.moviePreviews = (ArrayList<MoviePreview>) moviePreviews;
        notifyDataSetChanged();
    }


    public void defaultOrder() {
        replaceWith(defaultOrder);
        notifyDataSetChanged();
    }
}
