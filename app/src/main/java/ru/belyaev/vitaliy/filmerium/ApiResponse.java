package ru.belyaev.vitaliy.filmerium;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.belyaev.vitaliy.filmerium.domain.MoviePreview;

public class ApiResponse {

    @SerializedName("Search")
    @Expose
    private List<MoviePreview> moviePreviews = null;

    @SerializedName("totalResults")
    @Expose
    private String totalResults;
    @SerializedName("Response")
    @Expose
    private String response;

    public List<MoviePreview> getMoviePreviews() {
        return moviePreviews;
    }

    public String getTotalResults() {
        return totalResults;
    }

    public String getResponse() {
        return response;
    }

}