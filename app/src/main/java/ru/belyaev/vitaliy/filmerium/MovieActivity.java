package ru.belyaev.vitaliy.filmerium;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.belyaev.vitaliy.filmerium.domain.Movie;

import static ru.belyaev.vitaliy.filmerium.MainActivity.MOVIE_ID;

public class MovieActivity extends AppCompatActivity {

    public static final String LOG_TAG = MovieActivity.class.getName();

    private ImageView movieImage;
    private TextView title;
    private TextView director;
    private TextView runtime;
    private TextView year;
    private TextView country;
    private TextView genre;
    private TextView rating;
    private TextView actors;
    private TextView plot;
    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        initializeUI();

        String imdbID = getIntent().getStringExtra(MOVIE_ID);

        getApp().getOmdbApi().getMovie(imdbID).enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                if (response.code() == 200) {
                    movie = response.body();
                    setMovie(movie);
                    setToolbar(movie.getTitle());
                } else {
                    Log.e(LOG_TAG, "Error with code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.album_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent i = NavUtils.getParentActivityIntent(this);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                NavUtils.navigateUpTo(this, i);
                return true;

            case R.id.action_open_in_browser:
                if (!movie.getWebsite().equals("N/A")) {
                    Uri webpage = Uri.parse(movie.getWebsite());
                    CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();

                    builder
                            .setToolbarColor(ContextCompat.getColor(this, R.color.primaryColor))
                            .addDefaultShareMenuItem();

                    CustomTabsIntent customTabsIntent = builder.build();
                    customTabsIntent.launchUrl(this, webpage);
                    return true;
                } else {
                    Toast.makeText(this, getString(R.string.no_website), Toast.LENGTH_SHORT).show();
                }

        }
        return super.onOptionsItemSelected(item);
    }

    public void setToolbar(@NonNull String title) {
        setSupportActionBar((Toolbar) findViewById(R.id.movie_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsingToolbarLayout);
        collapsingToolbar.setTitle(title);
        collapsingToolbar.setCollapsedTitleTextColor(getResources().getColor(R.color.primaryTextColor));
        collapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.primaryTextColor));
    }

    private void setMovie(Movie movie) {

        Picasso.get()
                .load(movie.getPoster())
                .into(movieImage);

        title.setText(movie.getTitle());
        director.setText(movie.getDirector());
        runtime.setText(movie.getRuntime());
        year.setText(formatDate(movie.getReleased()));
        country.setText(movie.getCountry());
        genre.setText(movie.getGenre());
        actors.setText(movie.getActors());
        rating.setText(movie.getImdbRating());
        plot.setText(movie.getPlot());
    }

    private void initializeUI() {
        movieImage = findViewById(R.id.movie_poster_detailed);
        title = findViewById(R.id.tv_movie_title_value);
        director = findViewById(R.id.tv_movie_director_value);
        runtime = findViewById(R.id.tv_movie_runtime_value);
        year = findViewById(R.id.tv_movie_year_value);
        country = findViewById(R.id.tv_movie_country_value);
        genre = findViewById(R.id.tv_movie_genre_value);
        actors = findViewById(R.id.tv_movie_actors_value);
        rating = findViewById(R.id.tv_movie_rating_value);
        plot = findViewById(R.id.tv_movie_plot_value);
    }

    private String formatDate(String dateString) {
        try {
            DateFormat inputSdf = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
            DateFormat outputSdf = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
            Date date = inputSdf.parse(dateString);
            return outputSdf.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void start(Activity activity, String imdbID) {
        Intent intent = new Intent(activity, MovieActivity.class);
        intent.putExtra(MOVIE_ID, imdbID);
        activity.startActivity(intent);
    }

    private App getApp() {
        return ((App) getApplication());
    }
}
