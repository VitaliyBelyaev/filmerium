package ru.belyaev.vitaliy.filmerium;

import android.app.Application;

public class App extends Application {

    private OmdbApi omdbApi;

    @Override
    public void onCreate() {
        super.onCreate();

        NetworkModule networkModule = new NetworkModule(this);
        omdbApi = networkModule.getItunesApi();
    }

    public OmdbApi getOmdbApi() {
        return omdbApi;
    }
}
