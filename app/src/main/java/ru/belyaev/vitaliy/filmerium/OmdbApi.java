package ru.belyaev.vitaliy.filmerium;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.belyaev.vitaliy.filmerium.domain.Movie;

public interface OmdbApi {

    @GET("/?type=movie")
    Call<ApiResponse> searchMovies(@Query("s") String query);

    @GET("/?type=movie")
    Call<Movie> getMovie(@Query("i") String imdbID);
}
