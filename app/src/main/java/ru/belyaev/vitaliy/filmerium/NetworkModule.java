package ru.belyaev.vitaliy.filmerium;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static ru.belyaev.vitaliy.filmerium.Constants.OMDB_API_HOST;
import static ru.belyaev.vitaliy.filmerium.Constants.OMDB_API_KEY;
import static ru.belyaev.vitaliy.filmerium.Constants.OMDB_API_KEY_NAME;

public class NetworkModule {

    private static final long HTTP_CACHE_SIZE = 1024 * 1024 * 24L; // 24 MB
    private OmdbApi omdbApi;

    public NetworkModule(Context context) {
        final Interceptor interceptor = provideRequestInterceptor();
        final OkHttpClient okHttpClient = provideOkHttp(context.getCacheDir(), interceptor);
        final Retrofit apiRetrofit = provideRetrofit(okHttpClient, OMDB_API_HOST);
        omdbApi = apiRetrofit.create(OmdbApi.class);
    }

    public OmdbApi getItunesApi() {
        return omdbApi;
    }

    private OkHttpClient provideOkHttp(File cacheDir, Interceptor interceptor) {
        final HttpLoggingInterceptor.Logger logger = new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.d("OkHttp", message);
            }
        };

        final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(logger) //
                .setLevel(HttpLoggingInterceptor.Level.HEADERS);

        return new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .addInterceptor(loggingInterceptor)
                .cache(new Cache(new File(cacheDir, "http"), HTTP_CACHE_SIZE))
                .build();

    }

    private Retrofit provideRetrofit(OkHttpClient okHttpClient, String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    private Interceptor provideRequestInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                final Request originalRequest = chain.request();
                final HttpUrl newUrl = originalRequest.url().newBuilder()
                        .addQueryParameter(OMDB_API_KEY_NAME, OMDB_API_KEY)
                        .build();

                final Request newRequest = originalRequest.newBuilder()
                        .url(newUrl)
                        .addHeader("Accept", "application/json")
                        .build();

                return chain.proceed(newRequest);
            }
        };
    }

}
