# Filmerium
Within this app you can search movies trough OMDb API and then check detailed information about particular movie.

- Search is providing by Retrofit with  [OMDb API](http://www.omdbapi.com/)
- Results are displaying in RecyclerView
- When click on movie opens new Activity with detailed information about movie
- Clicking on "Open in browser" icon in album screen opens movie webpage in-app using Google Custom Tabs
